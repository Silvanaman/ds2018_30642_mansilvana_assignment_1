<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<%@ page isELIgnored="false" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<style>
    body {
        background-color: #00a79d;
    }

    #flightsTable {
        border-collapse: collapse;
        width: 70%;
        align: center;
    }

    #flightsTable td, #flightsTable th {
        border: 1px solid #ddd;
        padding: 8px;
        background-color: lightblue;
    }

    #flightsTable th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #7FB8AF;
        color: #0E433B;
    }

    .box {

        border: 2px solid black;
        background-color: #99ccff;
        font-weight: bold;
        padding: 12px;
        margin: 10px 20px;

    }

    .button {

        padding: 12px;
        border: 2px solid white;
        border-radius: 4px;
        margin: 10px 0;
        opacity: 0.85;
        display: inline-block;
        font-size: 17px;
        text-decoration: none;
    }

    #addUpdate {
        display: flex;
        padding: 12px;

    }
</style>
<head>
    <title>Hello, boss!</title>
</head>
<body>

<br/><br/>

<div>

    <form action="adminPage" method="get">

        <table id="flightsTable">
            <tr>
                <th>ID</th>
                <th>Arrival date</th>
                <th>Departure date</th>
                <th>Arrival city</th>
                <th>Departure city</th>
                <th>Type</th>
            </tr>

            <c:forEach var="flight" items="${flights}">

                <tr>
                    <td><c:out value="${flight.id}"/></td>

                    <td>
                        <c:out value="${flight.arrivalDate}"/>
                    </td>
                    <td>
                        <c:out value="${flight.departureDate}"/>
                    </td>
                    <td>
                        <c:out value="${flight.arrivalCity.name}"/>
                    </td>

                    <td>
                        <c:out value="${flight.departureCity.name}"/>
                    </td>
                    <td>
                        <c:out value="${flight.type}"/></td>
                </tr>

            </c:forEach>


        </table>
    </form>
</div>
<br>
<div id="addUpdate">
    <div class="box">
        <form action="adminPage" method="POST">

            Arrival city: <input type="text" name="arrival_city"/><br/><br/>
            Arrival date:<input type="datetime-local" name="arrival_date"/><br/><br/>
            Departure city: <input type="text" name="departure_city"/><br/><br/>
            Departure date: <input type="datetime-local" name="departure_date"/><br/><br/>
            Type: <input type="text" name="type"/><br/><br/>

            <input type="submit" value="Add flight" class="button"/>
        </form>
    </div>
    <br><br>
    <div class="box">
        <form action="adminPageUpdate" method="POST">
            Arrival city: <input type="text" name="arrival_city1"/><br/><br/>
            Arrival date:<input type="datetime-local" name="arrival_date1"/><br/><br/>
            Departure city: <input type="text" name="departure_city1"/><br/><br/>
            Departure date: <input type="datetime-local" name="departure_date1"/><br/><br/>
            Type: <input type="text" name="type1"/><br/><br/>

            Insert ID of flight you want to modify:
            <input type="text" name="id_flight"/><br>
            <input type="submit" value="Update" class="button"/>
        </form>
    </div>
</div>
<br>
<div class="box">
    <form action="adminPageDelete" method="POST">
        Insert ID of flight you want to delete:
        <input type="text" name="id_flight_del"/><br>
        <input type="submit" value="Delete" class="button"/>
    </form>
</div>
<form action="logout" method="POST">
    <input type="submit" value="Logout">
</form>
</body>
</html>