<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page isELIgnored="false" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<style>
    body {
        background-color: #00a79d;
    }

    #flightsTable {
        border-collapse: collapse;
        width: 70%;
        align: center;
    }

    #flightsTable td, #flightsTable th {
        border: 1px solid #ddd;
        padding: 8px;
        background-color: lightblue;
    }

    #flightsTable th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #7FB8AF;
        color: #0E433B;
    }

    #box {
        border: 2px solid black;
        background-color: #e6ffff;
        font-weight: bold;
    }

</style>
<head>
    <title>View flights</title>
</head>
<body>

<br/><br/>

<div>

    <form action="clientPage" method="GET" id="clientForm">

        <table id="flightsTable">
            <tr>
                <th>ID</th>
                <th>Arrival date</th>
                <th>Departure date</th>
                <th>Arrival city</th>
                <th>Departure city</th>
                <th>Type</th>
            </tr>

            <c:forEach var="flight" items="${flights}">

                <tr>
                    <td><c:out value="${flight.id}"/></td>

                    <td>
                        <c:out value="${flight.arrivalDate}"/>
                    </td>
                    <td>
                        <c:out value="${flight.departureDate}"/>
                    </td>
                    <td>
                        <c:out value="${flight.arrivalCity.name}"/>
                    </td>

                    <td>
                        <%--<button form="clientForm" formaction="localTimeServlet" formmethod="GET" type="submit">--%>
                            <c:out value="${flight.departureCity.name}"/>
                                <%--<input type="hidden" value="cityl" name="city" />--%>
                        <%--</button>--%>
                    </td>
                    <td>
                        <c:out value="${flight.type}"/></td>
                </tr>

            </c:forEach>


        </table>
    </form>


    <form action="localTimeServlet" method="GET" >

        <input type="submit" value="New York" name="city" />
        <input type="submit" value="San Francisco" name="city" />
        <input type="submit" value="Bucuresti" name="city" />
        <input type="submit" value="Cluj" name="city" />
        <input type="submit" value="Budapesta" name="city" />

    </form>


</div>
<br>
<form action="logout" method="POST">
    <input type="submit" value="Logout">
</form>
</body>
</html>
