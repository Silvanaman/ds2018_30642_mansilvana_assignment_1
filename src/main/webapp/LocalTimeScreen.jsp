<%@ page isELIgnored="false" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<style>
    body {
        background-color: #00a79d;
    }

    #box {

        border: 2px solid black;
        background-color: #e6ffff;
        font-weight: bold;
    }
</style>
<head>
    <title>LocalDateTime</title>
</head>
<body>

<div id="box">
    <p>Local date time is ${localTime}</p>
</div>

</body>
</html>
