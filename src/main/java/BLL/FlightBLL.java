package BLL;

import DAO.CityDAO;
import DAO.FlightDAO;
import entities.City;
import entities.Flight;

import java.time.LocalDateTime;
import java.util.List;

public class FlightBLL {
    private final FlightDAO flightDAO=new FlightDAO();
    private final CityDAO cityDAO=new CityDAO();
    public List<Flight> getAllFlights()
    {
        return flightDAO.getAllFlights();
    }

    public void insertFlight(String airplaneType, String departureCity, String arrivalCity, LocalDateTime departureDate ,LocalDateTime arrivalDate)
    {
        City departureCity1=cityDAO.getCityByName(departureCity);
        City arrivalCity1=cityDAO.getCityByName(arrivalCity);
        Flight flightEntity = new Flight(airplaneType,departureCity1,arrivalCity1,departureDate,arrivalDate);
        flightDAO.insertFlight(flightEntity);
    }

    public void deleteFlight(String flightId)
    {
        //FlightEntity flightEntity = flightDao.getFlightById(Integer.parseInt(flightId));
        flightDAO.deleteFlight(Integer.parseInt(flightId));
    }

    public void updateFlight(String flightId,String airplaneType, String departureCity, LocalDateTime departureDate, String arrivalCity, LocalDateTime arrivalDate)
    {
        Flight flightEntity = flightDAO.getFlightById(Integer.parseInt(flightId));
        //if(!airplaneType.equals(""))
            flightEntity.setType(airplaneType);
        //if(arrivalDate!=null)
            flightEntity.setArrivalDate(arrivalDate);
       // if(departureDate!=null)
            flightEntity.setDepartureDate(departureDate);
        //if (departureCity!=null){
            City departureCityNew=cityDAO.getCityByName(departureCity);
            flightEntity.setDepartureCity(departureCityNew);
        //}
       // if(arrivalCity!=null) {
            City arrivalCityNew = cityDAO.getCityByName(arrivalCity);
            flightEntity.setArrivalCity(arrivalCityNew);
        //}
        flightDAO.updateFlight(flightEntity);
    }

    public Flight getFlightById(String flightId)
    {
        return flightDAO.getFlightById(Integer.parseInt(flightId));
    }

}
