package DAO;

import entities.Flight;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

public class FlightDAO {
    private EntityManager entityManager;
    private EntityManagerFactory entityManagerFactory;

    public List<Flight> getAllFlights()
    {

        entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
        entityManager = entityManagerFactory.createEntityManager();
        Query query = entityManager.createQuery("select f FROM Flight as f");
        List<Flight> flightEntityList = query.getResultList();

        entityManagerFactory.close();
        return flightEntityList;
    }

    public Flight getFlightById(int flightId)
    {


        entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
        entityManager = entityManagerFactory.createEntityManager();
        Query query = entityManager.createQuery("select f FROM Flight as f where f.id =:id");
        query.setParameter("id",flightId);
        List<Flight> flightEntityList = query.getResultList();

        entityManagerFactory.close();
        return flightEntityList.get(0);
    }

    public void insertFlight(Flight flightEntity)
    {
        entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
        entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        entityManager.persist(flightEntity);
        entityManager.getTransaction().commit();

        entityManagerFactory.close();

    }

    public void updateFlight(Flight flightEntity)
    {
        entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
        entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        entityManager.merge(flightEntity);
        entityManager.getTransaction().commit();
        entityManagerFactory.close();

    }

    public void deleteFlight(int flightId)
    {
        entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
        entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        Query query = entityManager.createQuery("delete FROM Flight as f where f.id =:id");
        query.setParameter("id",flightId);
        query.executeUpdate();
        entityManager.getTransaction().commit();
        entityManagerFactory.close();

    }
}
