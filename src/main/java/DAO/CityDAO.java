package DAO;

import entities.City;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

public class CityDAO {
    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;
    public City getCityByName(String name)
    {
        entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
        entityManager = entityManagerFactory.createEntityManager();
        Query query = entityManager.createQuery("select c FROM City as c where c.name =:name");
        query.setParameter("name",name);
        List<City> cityEntityList = query.getResultList();

        entityManagerFactory.close();
        return cityEntityList.get(0);
    }
}
