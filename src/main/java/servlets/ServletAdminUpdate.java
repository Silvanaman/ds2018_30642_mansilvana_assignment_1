package servlets;

import BLL.FlightBLL;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;

@WebServlet(name = "ServletAdminUpdate",urlPatterns = "/adminPageUpdate")
public class ServletAdminUpdate extends HttpServlet {
    private final FlightBLL flightBLL=new FlightBLL();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String arrivalCity = request.getParameter("arrival_city1");
        String departureCity = request.getParameter("departure_city1");
        String type = request.getParameter("type1");
        LocalDateTime arrivalDate = LocalDateTime.parse(request.getParameter("arrival_date1"));
        LocalDateTime departureDate = LocalDateTime.parse(request.getParameter("departure_date1"));
        String idFlight=request.getParameter("id_flight");
        flightBLL.updateFlight(idFlight,type,departureCity,departureDate,arrivalCity,arrivalDate);
        response.sendRedirect("/adminPage");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
