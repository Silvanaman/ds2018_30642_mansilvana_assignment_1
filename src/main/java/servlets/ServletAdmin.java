package servlets;

import BLL.FlightBLL;
import entities.Flight;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

@WebServlet(name = "ServletAdmin", urlPatterns = "/adminPage")
public class ServletAdmin extends HttpServlet {
    private final FlightBLL flightBLL = new FlightBLL();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String arrivalCity = request.getParameter("arrival_city");
        String departureCity = request.getParameter("departure_city");
        String type = request.getParameter("type");
        LocalDateTime arrivalDate = LocalDateTime.parse(request.getParameter("arrival_date"));
        LocalDateTime departureDate = LocalDateTime.parse(request.getParameter("departure_date"));
        System.out.println("arrivalCity " + arrivalCity + " departureCity " + departureCity + " type " + type + " arrivaldate " + arrivalDate + " departureDate " + departureDate);
        flightBLL.insertFlight(type, departureCity, arrivalCity, departureDate, arrivalDate);
        response.sendRedirect("/adminPage");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Flight> flights = flightBLL.getAllFlights();
        request.setAttribute("flights", flights);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("adminPage.jsp");
        requestDispatcher.forward(request, response);
        // response.sendRedirect("adminPage.jsp");
    }



}
