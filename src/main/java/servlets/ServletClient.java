package servlets;

import BLL.FlightBLL;
import entities.Flight;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "ServletClient",urlPatterns = "/clientPage")
public class ServletClient extends HttpServlet {
    private final FlightBLL flightBLL=new FlightBLL();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Flight> flights = flightBLL.getAllFlights();
        request.setAttribute("flights", flights);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("clientPage.jsp");
        requestDispatcher.forward(request, response);

    }
}
