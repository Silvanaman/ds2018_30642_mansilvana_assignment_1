package servlets;

import BLL.UserBLL;
import DAO.UserDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/ServletLogin")
public class ServletLogin extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserBLL userBLL=new UserBLL();

        String username = request.getParameter("username");
        String password = request.getParameter("password");

        System.out.println(userBLL.logUser(username, password));
        int res=userBLL.logUser(username, password);
        System.out.println("rez==="+res);
        if ( res== 1) {
            Cookie message = new Cookie("AdminLogged", username);
            message.setMaxAge(30*60);
            response.addCookie(message);
            response.sendRedirect("/adminPage");
        } else if (res == 2) {
            Cookie message = new Cookie("ClientLogged", username);
            message.setMaxAge(30*60);
            response.addCookie(message);
            response.sendRedirect("/clientPage");
        } else if (res == -1) {
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();
            out.append("Wrong username or password");

        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
