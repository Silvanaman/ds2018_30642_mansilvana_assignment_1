package servlets;

import DAO.CityDAO;
import entities.City;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@WebServlet(name = "ServletLocalTime", urlPatterns = "/localTimeServlet")
public class ServletLocalTime extends HttpServlet {
    private final CityDAO cityDAO = new CityDAO();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String cityName = request.getParameter("city");
        System.out.println("coordonate pentru orasul "+cityName);
        City city = cityDAO.getCityByName(cityName);
        String localTime = null;
        try {
            String latitude = Double.toString(city.getLatitude());
            String longitude = Double.toString(city.getLongitude());
            System.out.println(latitude + "/" + longitude);
            String url = "http://www.new.earthtools.org/timezone/" + latitude + "/" + longitude;
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            int responseCode = con.getResponseCode();
            System.out.println("Response Code : " + responseCode);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            Pattern p = Pattern.compile("<localtime>(.*)</localtime>");

            while ((inputLine = in.readLine()) != null) {
                Matcher m = p.matcher(inputLine);
                if (m.find()) {
                    localTime = m.group(1);
                }
            }
            System.out.println(localTime);
            in.close();
        } catch (Exception e) {
            System.out.println(e);
        }

        request.setAttribute("localTime", localTime);
        request.getRequestDispatcher("LocalTimeScreen.jsp").forward(request, response);
    }
}
