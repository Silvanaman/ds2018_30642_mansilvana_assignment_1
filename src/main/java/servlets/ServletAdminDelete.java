package servlets;

import BLL.FlightBLL;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ServletAdminDelete",urlPatterns = "/adminPageDelete")
public class ServletAdminDelete extends HttpServlet {
    private final FlightBLL flightBLL=new FlightBLL();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String idFlight=request.getParameter("id_flight_del");
        flightBLL.deleteFlight(idFlight);
        response.sendRedirect("/adminPage");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
